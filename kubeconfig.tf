resource "local_sensitive_file" "kubeconfig" {
  count = length(var.cluster_access_token)

  content = templatefile("${path.module}/templates/kubeconfig.tpl", {
    name              = var.users_list[count.index].name
    endpoint          = var.cluster_endpoint
    cluster_ca_base64 = base64encode(var.cluster_ca_certificate)
    token             = var.cluster_access_token[count.index]
  })

  filename             = join("_", ["${var.kubeconfig_output_path}", "${var.cluster_name}", "${var.users_list[count.index].name}"])
  file_permission      = var.kubeconfig_file_permission
  directory_permission = var.kubeconfig_directory_permission
}

# Cluster
variable "cluster_name" {
  description = "Nome do cluster EKS. Utilizado como prefixo no nome de alguns recursos relacionados."
  type        = string
}

variable "cluster_endpoint" {
  description = "Hostname da API do Kubernetes."
  type        = string
}

variable "cluster_ca_certificate" {
  description = "Certificados codificados por PEM para autenticação TLS com a API do Kubernetes."
  type        = string
}

variable "cluster_access_token" {
  description = "Token do `service account` para autenticação na API do Kubernetes."
  type        = list(any)
}

# Kubeconfig
variable "kubeconfig_output_path" {
  description = "Caminho de criação do arquivo kubeconfig."
  type        = string
  default     = "./kube/kubeconfig"
}

variable "kubeconfig_file_permission" {
  description = "Permissão do arquivo kubeconfig que será criado."
  type        = string
  default     = "0600"
}

variable "kubeconfig_directory_permission" {
  description = "Permissão da pasta aonde kubeconfig que será criado."
  type        = string
  default     = "0755"
}

## Users
variable "users_list" {
  type = list(map(string))
}